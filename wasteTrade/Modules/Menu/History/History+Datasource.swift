//
//  History+Datasource.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 4/15/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

extension HistoryViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as? HistoryCell else {return HistoryCell()}
        setShadow(view: cell.cellView)
        roundView(view: cell.cellView, radius: CGFloat(5))
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(hex: "0x9AD98E")
        roundView(view: backgroundView, radius: CGFloat(5))
        cell.selectedBackgroundView = backgroundView

        return cell
    }
    
    
}
