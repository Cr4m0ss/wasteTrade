//
//  Buy&SellViewController.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 4/16/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

class BuyAndSellViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var buyOrSellButton: UIButton!
    
    //MARK: Variables & Constants
    var currentIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         setCustomBackButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        currentIndex = self.tabBarController?.selectedIndex ?? 0
        switch currentIndex {
        case 0:
            buyOrSellButton.setTitle("COMPRAR", for: .normal)
        case 1:
             buyOrSellButton.setTitle("PUBLICAR", for: .normal)
        default:()
        }
    }
    
    //MARK: Actions
    @IBAction func showHistory(_ sender: Any) {
        guard let vc = UIStoryboard(name: "History", bundle: nil).instantiateViewController(withIdentifier: "History") as? HistoryViewController  else {return}
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func goToBuy(_ sender: Any) {
        switch currentIndex {
        case 0:
            guard let vc = UIStoryboard(name: "Buy", bundle: nil).instantiateViewController(withIdentifier: "Buy") as? BuyViewController  else {return}
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            guard let vc = UIStoryboard(name: "Post", bundle: nil).instantiateViewController(withIdentifier: "Post") as? PostViewController  else {return}
            self.navigationController?.pushViewController(vc, animated: true)
        default:()
        }
    }
}
