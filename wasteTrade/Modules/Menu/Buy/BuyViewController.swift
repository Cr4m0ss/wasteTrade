//
//  BuyViewController.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 4/15/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

class BuyViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var greenGradient: UIImageView!
    
    //MARK: Variables & Constants
    var isShowingCustomView = false
    var currentButton = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        heightConstraint.constant = 60
        greenGradient.isHidden = true
        setUpNavigationBar()
    }
    
    func setUpNavigationBar(){
        let leftButton = UIBarButtonItem(title: "CLASIFICADOS", style: .plain, target: self, action: #selector(showClasificados))
        
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        button.setImage(UIImage(named: "searchButton"), for: .normal)
        button.addTarget(self, action: #selector(showSearch), for: .touchUpInside)
        
        let rightButton = UIBarButtonItem(title: "CATEGORIAS", style: .plain, target: self, action: #selector(showCategories))
        self.navigationItem.rightBarButtonItem = rightButton
        
        self.navigationItem.titleView = button
        
        let back = UIBarButtonItem(image: UIImage(named: "backButton"), landscapeImagePhone: nil, style: .plain, target: self, action: #selector(backButton))
        self.navigationItem.leftBarButtonItems = [back, leftButton]
    }
    
    
    func updateCustomView(isActive:Bool){
        if isShowingCustomView && isActive{
            isShowingCustomView = false
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                self.greenGradient.isHidden = true
                self.heightConstraint.constant = 60
                self.view.layoutIfNeeded()
            }, completion: nil)
        }else{
            isShowingCustomView = true
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                self.greenGradient.isHidden = false
                self.heightConstraint.constant = 140
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func showClasificados(){
        if currentButton == 1 {
            updateCustomView(isActive: true)
        }else{
            currentButton = 1
            updateCustomView(isActive: false)
        }
        
    }
    
    @objc func showSearch(button: UIButton) {
        if currentButton == 2 {
            updateCustomView(isActive: true)
        }else{
            currentButton = 2
            updateCustomView(isActive: false)
        }
    }
    
    @objc func showCategories(){
        if currentButton == 3 {
            updateCustomView(isActive: true)
        }else{
            currentButton = 3
            updateCustomView(isActive: false)
        }
    }
    
 
    
    //MARK: Actions
}
