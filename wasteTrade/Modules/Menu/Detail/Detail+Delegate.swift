//
//  Detail+Delegate.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 4/16/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

extension DetailViewController: UIScrollViewDelegate, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = indexPath.row
        switch  row {
        case 0:
            return UITableViewAutomaticDimension
        default:
            return 60
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
