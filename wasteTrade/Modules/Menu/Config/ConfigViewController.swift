//
//  ConfigViewController.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 8/21/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

class ConfigViewController: UIViewController {
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.tableFooterView = UIView()
    }
}
