//
//  CommentsViewController.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 4/16/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

class CommentsViewController: UIViewController {
    
    //MARK: Outlets
    
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var sendCommentView: UIView!
    @IBOutlet weak var comment: UITextField!
    
    //MARK: Variables & Constants
    let radius = CGFloat(25)
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setUpViews()
    }
    
    func setUpViews(){
        roundView(view: commentView, radius: radius)
        roundView(view: sendCommentView, radius: radius)
        comment.autocorrectionType = .no

    }
    
    //MARK: Actions
    @IBAction func sendMessage(_ sender: Any) {
        print("send")
    }
}
