//
//  LoginViewController.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 4/15/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController{
    
    //MARK: OutLets
    @IBOutlet weak var loginBtnView: UIView!
    @IBOutlet weak var facebookBtnView: UIView!
    @IBOutlet weak var gMailBtnView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var simpleLoginBtn: UIButton!
    @IBOutlet weak var facebookLoginBtn: UIButton!
    @IBOutlet weak var gmailLoginBtn: UIButton!
    @IBOutlet weak var signUp: UIButton!
    @IBOutlet weak var userLblView: UIView!
    @IBOutlet weak var passWordLblView: UIView!
    @IBOutlet weak var passwordLabel: UITextField!
    @IBOutlet weak var userLabel: UITextField!
    
    @IBOutlet weak var verticalSpaceConstraint: NSLayoutConstraint!
    //MARK: Variables & Constants
    let cornerRadius = CGFloat(15)
    let manager = Managers()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transparentNavigationBar()
        setUpViews()
        hideKeyboardWhenTappedAround()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyBoardWillShow(notification: NSNotification) {
        UIView.animate(withDuration: 3, delay: 0, options: .curveEaseOut, animations: {
            self.topConstraint.constant = -120
            self.verticalSpaceConstraint.constant = 10
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 3, delay: 0, options: .curveEaseOut, animations: {
            self.topConstraint.constant = -30
            self.verticalSpaceConstraint.constant = 98
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    func transparentNavigationBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func setUpViews(){
        roundView(view: loginBtnView, radius: cornerRadius)
        roundView(view: facebookBtnView, radius: cornerRadius)
        roundView(view: gMailBtnView, radius: cornerRadius)
        roundView(view: userLblView, radius: cornerRadius)
        roundView(view: passWordLblView, radius: cornerRadius)
        
        setShadow(view: loginBtnView)
        setShadow(view: facebookBtnView)
        setShadow(view: gMailBtnView)
        
        userLabel.autocorrectionType = .no
        passwordLabel.autocorrectionType = .no
        
        setCustomBackButton()
    }
    
    //MARK: IBActions
    @IBAction func login(_ sender: UIButton) {
        switch sender {
        case simpleLoginBtn:
            guard let vc = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "TabBar") as? TabBarViewController  else {return}
            manager.loginUser(email: userLabel.text, password: passwordLabel.text) { success, result in
                if success {
                    self.navigationController?.pushViewController(vc, animated: true)
                }else {
                    print(":(")
                }
            }
        case facebookLoginBtn:
            print("facebookLogin")
        case gmailLoginBtn:
            print("gmailLogin")
        default:()
        }
    }

    @IBAction func signUp(_ sender: Any) {
        guard let vc = UIStoryboard(name: "SignUp", bundle: nil).instantiateViewController(withIdentifier: "SignUp") as? SignUpViewController else {return}
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func forgottenPassword(_ sender: Any) {
        print("forgotten password")
    }
    
}
