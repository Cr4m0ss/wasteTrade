//
//  SignUpViewController.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 4/15/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    //MARK: OutLets
    @IBOutlet weak var facebookBtnView: UIView!
    @IBOutlet weak var gMailBtnView: UIView!
    
    //MARK: Variables & Constants
    let cornerRadius = CGFloat(15)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
    }
    
    func setUpViews(){
        roundView(view: facebookBtnView, radius: cornerRadius)
        roundView(view: gMailBtnView, radius: cornerRadius)
        
        setShadow(view: facebookBtnView)
        setShadow(view: gMailBtnView)
    }
}
