//
//  WelcomeViewController.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 4/15/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var welcomeTextLbl: UILabel!
    @IBOutlet weak var acceptBtnView: UIView!
    @IBOutlet weak var acceptButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
    }
    
    func setUpViews(){
        roundView(view: acceptBtnView, radius: CGFloat(15))
    }
    
    //MARK: IBActions
    
    @IBAction func accept(_ sender: Any) {
        
    }
    
}
