//
//  Manager.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 8/20/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import Foundation
import Moya

class Managers {
    private let response = ResponseMapping()
    private let provider = MoyaProvider<WasteTrade>()
    
    func loginUser(email: String!, password: String!, _ completion : @escaping(_ success: Bool, _ object: Any?) -> Void) {
        provider.request(.login(email: email, password: password)) { result in
            switch result {
            case .success(let response):
                guard let jsonString = try? response.mapString() else {return completion(false, nil)}
                self.response.parseLogin(stringResponse: jsonString, completion: completion)
            case .failure(let error):
                print("Error request: \(error.errorDescription ?? "")")
                completion(false, error)
            }
        }
    }
    
    func getUser( _ completion : @escaping(_ success: Bool, _ object: Any?) -> Void) {
        provider.request(.getUser) { result in
            switch result {
            case .success(let response):
                guard let jsonString = try? response.mapString() else {return completion(false, nil)}
                print(jsonString)
                completion(true, jsonString)
            case .failure(let error):
                print("Error request: \(error.errorDescription ?? "")")
                completion(false, error)
            }
        }
    }
}
