//
//  WastetradeEndpoints.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 8/20/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import Foundation
import Moya

enum WasteTrade {
    case login(email:String, password:String)
    case getUser
}

extension WasteTrade: TargetType {
    
    var baseURL: URL { return URL(string: Constants.baseURL)! }
    
    var path: String {
        switch self {
        case .login:
            return "auth/local"
        case .getUser:
            return "api/users/me"
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .login:
            return ["Content-Type":"application/json"]
        default:
            return ["Content-Type":"application/json","Authorization":"Bearer \(Persistance().getToken() ?? "")"]
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .login:
            return . post
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .login(let user, let password):
            return .requestParameters(parameters: ["email":user,"password":password], encoding: JSONEncoding.default)
        default:
            return .requestPlain
        }
    }
}
