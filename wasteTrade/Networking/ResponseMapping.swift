//
//  ResponseMapping.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 8/20/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import Foundation

class ResponseMapping {
    
    func serializeResponse(withData responseData: String) -> [String:Any]? {
        if responseData.data(using: .utf8) != nil {
            do {
                let data = responseData.data(using: .utf8)
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                return jsonData
            } catch {
                print(error.localizedDescription)
                return nil
            }
        } else {
            return nil
        }
    }
    
    func parseLogin(stringResponse: String, completion: @escaping(_ success: Bool, _ object: Any?) -> Void) {
        guard let dataJSON = serializeResponse(withData: stringResponse) else {
            return completion(false, nil)
        }
        guard let responseDictionary = dataJSON["token"] as? String else {
            return completion(false, nil)
        }
        Persistance().setToken(data: responseDictionary)
        completion(true,responseDictionary)
    }
}
